set nocp

call pathogen#infect()

if has("gui_running")
  set background=light
  if has("gui_macvim")
    set guifont=Monaco:h14
  endif
else
  set background=dark
endif

filetype plugin indent on
filetype detect
syntax on

set autoindent
set backspace=indent,eol,start

set tw=72
set wrap
set linebreak
set showbreak=…
set ignorecase

set expandtab
set sw=2
set sts=2
set ts=8

set showcmd
set ruler
set statusline=%<%f\ 0x%02B\ %h%m%r%=%-14.(%l,%c%V%)\ %P

set incsearch
set hlsearch
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

nmap  :tabp<CR>
nmap  :tabn<CR>

au BufNewFile,BufRead *.cl set filetype=c
au BufNewFile,BufRead *.pl set filetype=prolog
"au BufNewFile,BufRead *.m set filetype=octave
au BufNewFile,BufRead *.m set filetype=objc
au BufNewFile,BufRead,BufReadPost *.md set filetype=markdown

" par
set formatprg=par\ -w72q
set fo=

" allow editing crontab
" http://vim.wikia.com/wiki/Editing_crontab
set backupskip=/tmp/*,/private/tmp/*

set nobackup
set nowritebackup

" edit shortcuts
let mapleader = ","
cnoremap %% <C-R>=expand('%:h').'/'<CR>
map <leader>ew :e %%

" open/close fold using <space>
nnoremap <space> za

map <c-k> kddpk
map <c-j> ddp

fu! InsertTimestamp()
  r! date +\[\%Y/\%m/\%d\ \%H:\%M:\%S\]
endfu
command! Timestamp call InsertTimestamp()

" hdevtools
au FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
au FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsClear<CR>
au FileType haskell nnoremap <buffer> <silent> <F3> :HdevtoolsInfo<CR>

" localvimrc
let g:localvimrc_name=".vimrc"
let g:localvimrc_ask=0
let g:localvimrc_sandbox=0

" php syntax
let g:php_htmlInStrings=1

" vim-better-whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
